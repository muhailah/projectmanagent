//
//  TaskViewController.swift
//  ProjectManagent
//
//  Created by Muhailah AlSahali on 25/03/2020.
//  Copyright © 2020 Muhailah AlSahali. All rights reserved.
//

import UIKit

class TaskViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
 
    @IBOutlet weak var generateCostBtn: UIButton!
    @IBOutlet weak var taskTable: UITableView!
    @IBOutlet weak var addBtn: UIBarButtonItem!
    @IBOutlet weak var cancelBtn: UIBarButtonItem!
    var taskName : UITextField?
    var taskResource : UITextField?
    var taskCost : UITextField?
    var tasks = [Task]()
    var project : Project?
    var cost = Double()
    override func viewDidLoad() {
        super.viewDidLoad()
        taskTable.dataSource = self
        taskTable.delegate = self
        taskTable.reloadData()
        taskTable.rowHeight = 80;
        Utilities.styleFilledButton(button: generateCostBtn)
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

    
    }
    

    //Tableview setup
//    func numberOfSections(in tableView: UITableView) -> Int {
//        return 1
//    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      
        print("Tableview setup \(tasks.count)")
        return tasks.count
        
    }
     
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
         let cell = taskTable.dequeueReusableCell(withIdentifier: "TaskListItem", for: indexPath) as! TaskTableViewCell
        
        cell.setTask(task: tasks[indexPath.row])
    

        
        print("Array is populated \(tasks)")


         return cell
     }
     

     @IBAction func cancelBtn(_ sender: Any) {
         
         let alertController = UIAlertController(title:"Are you sure?", message: "You will lose your prject", preferredStyle:.alert)
         
         
         alertController.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (action: UIAlertAction!) in
             self.performSegue(withIdentifier: "TaskToProject", sender: self)
         }))
         
        alertController.addAction(UIAlertAction(title: "No", style: .cancel))
         self.present(alertController, animated: true, completion: nil)

          
     }
    @IBAction func addBtn(_ sender: Any) {
         let alertController = UIAlertController(title:"Add a task to your project", message: nil, preferredStyle:.alert)
        alertController.addTextField(configurationHandler: taskName)
        alertController.addTextField(configurationHandler: taskResource)
        alertController.addTextField(configurationHandler: taskCost)
        let okAction = UIAlertAction(title: "Add", style: .default, handler: self.addHandler)
        let cancleAction = UIAlertAction(title: "Cancle", style: .cancel, handler: nil)
        alertController.addAction(okAction)
        alertController.addAction(cancleAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    @IBAction func generateCost(_ sender: Any) {
        for task in tasks {
            cost += task.taskCost
        }
       
         self.performSegue(withIdentifier: "TasksToFinalScreen", sender: self)
        
    }
    func taskName (textField: UITextField){
        taskName = textField
        taskName?.placeholder = "Enter task name"
    }
    func taskResource(textField: UITextField){
          taskResource = textField
        taskResource?.placeholder = "Enter task resource"

      }
    func taskCost(textField: UITextField){
        //validation or not ?
        taskCost = textField
        taskCost?.placeholder = "Enter task cost"
        taskCost!.keyboardType = .numberPad
    }
    func addHandler(alert :UIAlertAction){
        //let cost = Double(taskCost?.text)
        let name =  (taskName!.text!)
        let res = (taskResource!.text!)
        let cost = Double(taskCost!.text!)
        let taskCell = Task(taskName: name, taskResource: res , taskCost: cost!)
        tasks.append(taskCell)
        print(tasks)
        taskTable.reloadData()
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "TasksToFinalScreen"{
           let destnationVC = segue.destination as! ProjectDetailsViewController
            destnationVC.project = project
            destnationVC.totalCost = cost
            destnationVC.modalPresentationStyle = .fullScreen
        }
        else {
            let destnationVC = segue.destination as! ViewController
            destnationVC.modalPresentationStyle = .fullScreen
        }
    }
}

extension LosslessStringConvertible {
       var string: String { .init(self) }
   }
