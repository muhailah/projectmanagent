//
//  Project.swift
//  ProjectManagent
//
//  Created by Muhailah AlSahali on 28/03/2020.
//  Copyright © 2020 Muhailah AlSahali. All rights reserved.
//

import Foundation
struct Project {
    var name : String
    var sDate : Date
    var eDate : Date
    var manager :String
    var cost : Double
}
