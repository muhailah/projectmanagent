//
//  ViewController.swift
//  ProjectManagent
//
//  Created by Muhailah AlSahali on 25/03/2020.
//  Copyright © 2020 Muhailah AlSahali. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITextFieldDelegate{
    
    @IBOutlet weak var ProjectName: UITextField!
    @IBOutlet weak var taskBtn: UIButton!
    @IBOutlet weak var ProjectCost: UITextField!
    @IBOutlet weak var ProjectManager: UITextField!
    @IBOutlet weak var startDate: UIDatePicker!
    @IBOutlet weak var endDate: UIDatePicker!
    var project : Project?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        ProjectName.delegate = self
        ProjectCost.delegate = self
        ProjectManager.delegate = self
        Utilities.styleFilledButton(button: taskBtn)
    }
    
    
    
    
    
    @IBAction func addTask(_ sender: Any) {
        if ProjectCost.text?.isEmpty ?? false || ProjectName.text?.isEmpty ?? false || ProjectManager.text?.isEmpty ?? false  {
            let alertController = UIAlertController(title:"Missing", message: "Fill all Fileds", preferredStyle:.alert)
                    
                   alertController.addAction(UIAlertAction(title: "OK", style: .cancel))
                    self.present(alertController, animated: true, completion: nil)
        }
        else{
        let cost = Double(ProjectCost.text!)
        project = Project(name: ProjectName.text!, sDate: startDate.date, eDate: endDate.date, manager: ProjectManager.text!, cost: cost!)

        
        self.performSegue(withIdentifier: "ProjectToTask", sender: self)
        
        }
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ProjectToTask"{
            let destnationVC = segue.destination as! TaskViewController
            //print(project)
            destnationVC.project = project
            destnationVC.modalPresentationStyle = .fullScreen
        }
    }
}

