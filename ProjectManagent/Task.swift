//
//  Task.swift
//  ProjectManagent
//
//  Created by Raghad Almatrodi on 3/27/20.
//  Copyright © 2020 Muhailah AlSahali. All rights reserved.
//

import Foundation
struct Task {
    var taskName: String
    var taskResource : String
    var taskCost : Double
    init(taskName: String, taskResource: String, taskCost: Double) {
        self.taskName  = taskName
        self.taskResource = taskResource
        self.taskCost = taskCost
    }
}

