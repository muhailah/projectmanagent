//
//  ProjectDetailsViewController.swift
//  ProjectManagent
//
//  Created by Raghad Almatrodi on 3/27/20.
//  Copyright © 2020 Muhailah AlSahali. All rights reserved.
//

import UIKit

class ProjectDetailsViewController: UIViewController {
    
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var startDate: UILabel!
    @IBOutlet weak var endDate: UILabel!
    @IBOutlet weak var manager: UILabel!
    @IBOutlet weak var cost: UILabel!
    var totalCost = Double()
    @IBOutlet weak var actualCost: UILabel!
    var project : Project?
    @IBOutlet weak var BudgetStatus: UILabel!
    @IBOutlet weak var finishBtn: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        name.text = project?.name
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let myString = formatter.string(from: project!.sDate) // string purpose I add here
        let yourDate = formatter.date(from: myString)
        formatter.dateFormat = "dd-MMM-yyyy"
        let myStringafd = formatter.string(from: yourDate!)
        
        let myString1 = formatter.string(from: project!.eDate) // string purpose I add here
        let yourDate1 = formatter.date(from: myString1)
        formatter.dateFormat = "dd-MMM-yyyy"
        let myStringafd1 = formatter.string(from: yourDate1!)
        startDate.text = myStringafd
        endDate.text = myStringafd1
        manager.text = project?.manager
        cost.text = project?.cost.tring
        actualCost.text = totalCost.string
        
        if project!.cost > totalCost {
            BudgetStatus.text = "Under Budget :D"
            BudgetStatus.textColor = UIColor.systemGreen
        } else if project!.cost == totalCost {
            BudgetStatus.text = "On Budget :)"
            BudgetStatus.textColor = UIColor.systemYellow
        } else if project!.cost < totalCost {
            BudgetStatus.text = "Over Budget!"
            BudgetStatus.textColor = UIColor.systemRed
        }
        Utilities.styleFilledButton(button: finishBtn)
        // Do any additional setup after loading the view.
    }
    
    @IBAction func finish(_ sender: Any) {
        self.performSegue(withIdentifier: "DetailsToFirst", sender: self)
        
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "DetailsToFirst"{
            let destnationVC = segue.destination as! ViewController
            destnationVC.modalPresentationStyle = .fullScreen
        }
    }
    
}
extension LosslessStringConvertible {
    var tring: String { .init(self) }
}
