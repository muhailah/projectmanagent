//
//  TaskTableViewCell.swift
//  ProjectManagent
//
//  Created by Raghad Almatrodi on 3/25/20.
//  Copyright © 2020 Muhailah AlSahali. All rights reserved.
//

import UIKit

class TaskTableViewCell: UITableViewCell {

    @IBOutlet weak var taskName: UILabel!
    @IBOutlet weak var taskResources: UILabel!
    
    @IBOutlet weak var taskCost: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func setTask(task :Task){
        let name = task.taskName
        let res = task.taskResource
        let cost = Double(task.taskCost)
        
        taskName.text = name
        taskResources.text = res
        taskCost.text = cost.string
    }
}
